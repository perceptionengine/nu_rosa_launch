import sys
import rosbag
import subprocess
import time
import datetime
import rospy

import yaml
import argparse


def write_data(topic, msg, t, out_bag):
    out_msg = msg
    out_bag.write(topic, out_msg, t)


def get_bag_times(bag_file):
    info_dict = yaml.load(
        subprocess.Popen(['rosbag', 'info', '--yaml', bag_file], stdout=subprocess.PIPE).communicate()[0])
    bag_duration = info_dict['duration']
    start_time = info_dict['start']
    end_time = info_dict['end']
    return int(bag_duration), start_time, end_time


def main(args):
    parser = argparse.ArgumentParser(description='Multi Rosbag fix.')
    parser.add_argument('in_file_a', nargs=1, help='input bag file')
    parser.add_argument('in_file_b', nargs=1, help='input bag file')
    parser.add_argument('out_file', nargs='+', help='output_bag_file')

    args = parser.parse_args()

    bagfile_a = args.in_file_a[0]
    bagfile_b = args.in_file_b[0]
    out_file = args.out_file[0]

    print(datetime.datetime.now(), "Reading input files")
    filename = out_file

    print(datetime.datetime.now(), "Trying to open output file {}".format(filename))

    outbag = rosbag.Bag(filename, 'w')
    print(datetime.datetime.now(), "Reading messages from file {}".format(bagfile_a))
    gene_a = rosbag.Bag(bagfile_a).read_messages()
    print(datetime.datetime.now(), "Reading messages from file {}".format(bagfile_b))
    gene_b = rosbag.Bag(bagfile_b).read_messages()
    data_a_ok = True
    data_b_ok = True
    t_start = time.clock()
    image_count = 1
    initial_pandar_time = None
    while True:
        try:
            topic_a, msg_a, time_a = gene_a.next()
        except StopIteration:
            data_a_ok = False

        try:
            topic_b, msg_b, time_b = gene_b.next()
        except StopIteration:
            data_b_ok = False

        if not data_a_ok and not data_b_ok:
            break

        if initial_pandar_time is None and "pandar" in topic_a:
            initial_pandar_time = msg_a.header.stamp                   # initial time

        if initial_pandar_time is not None and "imx_wide" in topic_b:
            new_camera_time = initial_pandar_time + rospy.Duration.from_sec(1/6.2)*image_count  # 6.2 hz
            image_count += 1
            msg_b.header.stamp = new_camera_time
            write_data(topic_b, msg_b, new_camera_time, outbag)

        if "pandar" in topic_a:
            write_data(topic_a, msg_a, time_a, outbag)

    print(datetime.datetime.now(), "Finished, indexing output file")
    outbag.close()
    print(datetime.datetime.now(), "total process time: ", (time.clock()-t_start), "m")


if __name__ == "__main__":
    main(sys.argv[1:])
