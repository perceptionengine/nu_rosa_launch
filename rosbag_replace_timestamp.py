import rospy
import rosbag
import os
import sys
import argparse


def rpl_msg_time_with_hdr(inbag, outbag):
    rospy.loginfo('Processing input bagfile  : %s', inbag)
    rospy.loginfo('Writing to output bagfile : %s', outbag)
    outbag = rosbag.Bag(outbag, 'w')
    pandar_t = None

    for topic, msg, t in rosbag.Bag(inbag, 'r').read_messages():
        if pandar_t is not None and "imx264" in topic:
            msg.header.stamp = pandar_t
        if "pandar" in topic:
            pandar_t = msg.header.stamp
        outbag.write(topic, msg, t)

    rospy.loginfo('Closing output bagfile and exit...')
    outbag.close()


if __name__ == "__main__":
    rospy.init_node('rpl_msg_time_with_hdr', anonymous=True)
    parser = argparse.ArgumentParser(
        description='Create a new bagfile from an existing one replacing the message time for the header time.')
    parser.add_argument('-o', metavar='OUTPUT_BAGFILE', required=True, help='output bagfile')
    parser.add_argument('-i', metavar='INPUT_BAGFILE', required=True, help='input bagfile')
    args = parser.parse_args()
    try:
        rpl_msg_time_with_hdr(args.i, args.o)
    except Exception, e:
        import traceback

        traceback.print_exc()
