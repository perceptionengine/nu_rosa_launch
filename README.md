# Rosa Launch

Only Blackfly included.


Clone with submodules:

`git clone --recursive git@gitlab.com:perceptionengine/nu_rosa_launch.git`



### How to use

BF only

```
rosparam set use_sim_time true
roslaunch rosa_launch full_launch.launch
rosbag play ROSA_ROSBAG.bag --clock /blackfly/camera_info:=/null/binfo /tf:=/null/tf
```

